# HMRC Checkout

I have followed the TDD approach while coding and there is an Application boostrap class called `Runner.scala`
that can be launched to start the application.

The input string can be modified at leisure.

Finally, there are two tags `step1` and `step2` that can be checked out to verify the thought process.
