package com.till

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should

class ParserSpec extends AnyFlatSpec with should.Matchers {
  "parseInput" should "be able to match Apple products" in {
    val products: List[Product] = Parser.parseInput("apple")
    products.size should equal(1)
    val firstProduct = products.head
    firstProduct shouldBe an [Apple]
    firstProduct.name should equal("apple")
  }

  it should "be able to match Orange products" in {
    val products: List[Product] = Parser.parseInput("orange")
    products.size should equal(1)
    val firstProduct = products.head
    firstProduct shouldBe an [Orange]
    firstProduct.name should equal("orange")
  }

  it should "be able to match multiple apples" in {
    val products: List[Product] = Parser.parseInput("apple apple")
    products.size should equal(2)
    val secondProduct = products(1)
    secondProduct shouldBe an [Apple]
    secondProduct.name should equal("apple")
  }

  it should "be able to match multiple oranges" in {
    val products: List[Product] = Parser.parseInput("orange orange")
    products.size should equal(2)
    val secondProduct = products(1)
    secondProduct shouldBe an [Orange]
    secondProduct.name should equal("orange")
  }

  it should "be able to match different products at the same time" in {
    val products: List[Product] = Parser.parseInput("orange apple")
    products.size should equal(2)
    val firstProduct = products.head
    firstProduct shouldBe an [Orange]
    firstProduct.name should equal("orange")
    val secondProduct = products(1)
    secondProduct shouldBe an [Apple]
    secondProduct.name should equal("apple")
  }

  it should "skip unknown product" in {
    val products: List[Product] = Parser.parseInput("orange potato apple")
    products.size should equal(2)
    val firstProduct = products.head
    firstProduct shouldBe an [Orange]
    firstProduct.name should equal("orange")
    val secondProduct = products(1)
    secondProduct shouldBe an [Apple]
    secondProduct.name should equal("apple")
  }
}
