package com.till

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should

class OfferCalculatorSpec extends AnyFlatSpec with OfferCalculator with should.Matchers {
  "applyAppleOffer" should "return no discount if only one apple in basket" in {
    val offer = applyAppleOffer(List(Apple()))

    offer should equal(0)
  }

  it should "return zero if no products in the basket" in {
    val offer = applyAppleOffer(List.empty)

    offer should equal(0)
  }

  it should "return zero if no Apple in the basket" in {
    val offer = applyAppleOffer(List.fill(5)(Orange()))

    offer should equal(0)
  }

  it should "return one discount if two apples in basket" in {
    val offer = applyAppleOffer(List.fill(2)(Apple()))

    offer should equal(.6)
  }

  it should "return one discount if three apples in basket" in {
    val offer = applyAppleOffer(List.fill(3)(Apple()))

    offer should equal(.6)
  }

  it should "return two discounts if five apples in basket" in {
    val offer = applyAppleOffer(List.fill(5)(Apple()))

    offer should equal(1.2)
  }

  "applyOrangeOffer" should "return zero if no product in basket" in {
    val offer = applyOrangeOffer(List.empty)

    offer should equal(0)
  }

  it should "return zero offer if no Orange in basket" in {
    val offer = applyOrangeOffer(List.fill(3)(Apple()))

    offer should equal(0)
  }

  it should "return zero offer if only two Oranges in basket" in {
    val offer = applyOrangeOffer(List.fill(2)(Orange()))

    offer should equal(0)
  }

  it should "return one offer if three Oranges in basket" in {
    val offer = applyOrangeOffer(List.fill(3)(Orange()))

    offer should equal(0.25)
  }

  it should "return two offers if six Oranges in basket" in {
    val offer = applyOrangeOffer(List.fill(6)(Orange()))

    offer should equal(0.5)
  }
}
