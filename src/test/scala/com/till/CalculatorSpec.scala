package com.till

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should

class CalculatorSpec extends AnyFlatSpec with should.Matchers {
  "calculate" should "Add the sum of apples" in {
    val price = Calculator.calculate(List(Apple(), Orange()))
    price should equal(0.85)
  }

  it should "calculate apples and oranges mixed in any orders" in { // actual example
    val price = Calculator.calculate(List(Apple(), Apple(), Orange(), Apple()))
    price should equal(2.05)
  }

  it should "remove any offers if available" in {
    def removeOnePound(products: List[Product]): BigDecimal = {
      BigDecimal(1)
    }
    val price = Calculator.calculate(List(Apple(), Apple(), Orange(), Apple()), List(removeOnePound))
    price should equal(1.05)
  }
}
