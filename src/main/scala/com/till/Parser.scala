package com.till

object Parser {
  def parseInput(input: String): List[Product] = {
    input.split(" ").map {
      case "apple" => Option(Apple())

      case "orange" => Option(Orange())

      case _ => None
    }.filter(_.isDefined)
      .map(_.get)
      .toList
  }

}
