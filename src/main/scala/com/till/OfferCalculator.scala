package com.till

import scala.util.Try

trait OfferCalculator {
  def applyAppleOffer(products: List[Product]): BigDecimal = {
    Try(products.count(_.isInstanceOf[Apple]) / 2)
      .map(numberOfOffer => numberOfOffer.floor * Apple().price)
      .getOrElse(0)
  }

  def applyOrangeOffer(products: List[Product]): BigDecimal = {
    Try(products.count(_.isInstanceOf[Orange]) / 3)
      .map(numberOfOffer => numberOfOffer.floor * Orange().price)
      .getOrElse(0)
  }
}
