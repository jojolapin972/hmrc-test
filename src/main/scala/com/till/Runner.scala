package com.till

object Runner extends App with OfferCalculator {
  val input = "apple apple orange apple orange apple apple"

  private val products: List[Product] = Parser.parseInput(input)

  private val price: BigDecimal = Calculator.calculate(products, List(applyAppleOffer, applyOrangeOffer))

  println(s"the price of $input is £ $price")
}
