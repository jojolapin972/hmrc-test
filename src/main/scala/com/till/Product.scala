package com.till

trait Product {
  val name: String
  val price: BigDecimal
}

case class Apple() extends Product {
  override val name: String = "apple"
  override val price: BigDecimal = 0.6
}

case class Orange() extends Product {
  override val name: String = "orange"
  override val price: BigDecimal = 0.25
}
