package com.till

object Calculator {
  def calculate(products: List[Product], offerCalculators: List[List[Product] => BigDecimal] = Nil): BigDecimal = {
    products.map(_.price)
      .sum
      .-(
        offerCalculators
          .map(_(products))
          .sum
      )
  }
}
